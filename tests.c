#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>

#include "somme.c" // Incluez votre fichier source

void test_somme() {
    CU_ASSERT(somme(3, 5) == 8);
    CU_ASSERT(somme(0, 0) == 0);
    CU_ASSERT(somme(-1, 1) == 0);
    // Ajoutez d'autres tests ici
}

int main() {
    CU_initialize_registry();

    CU_pSuite suite = CU_add_suite("Suite_de_tests_somme", NULL, NULL);
    CU_ADD_TEST(suite, test_somme);

    CU_basic_set_mode(CU_BRM_VERBOSE);
    CU_basic_run_tests();
    CU_cleanup_registry();

    return CU_get_error();
}
