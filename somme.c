#include <stdio.h>

int somme(int a, int b) {
    return a + b;
}

int main() {
    int result = somme(3, 5);
    printf("La somme est : %d\n", result);
    return 0;
}
